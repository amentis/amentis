#!/usr/bin/env bash
cp ~/.zshrc .
cp ~/.vimrc .

mv vim.tar.xz vim.tar.xz.bckp
pt="$(pwd)"
set -e
cd
tar -cvJf "${pt}/vim.tar.xz" .vim
cd "${pt}"
