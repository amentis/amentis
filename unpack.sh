#!/usr/bin/env bash
cp .zshrc ~/
cp .vimrc ~/

mv ~/.vim ~/.vim.bckp
tar -xvJf vim.tar.xz ~/.vim

set -e
pt="$(pwd)"
cd
tar -xvJf "${pt}/vim.tar.xz"
cd "${pt}"
