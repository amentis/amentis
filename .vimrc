"pathogen plugin installation
execute pathogen#infect()

:set encoding=utf-8
:set fileencoding=utf-8

syntax on
"filetype plugin indent on
:set guioptions-=T
:set guioptions-=m
:set guioptions-=r
:set guioptions-=L
:colorscheme molokai_dark
"number toggle
"if you want relative number by default
"set relativenumber
"set to abosulte numerbing
set number
"CTRL-n to toggle numbers
nnoremap <silent> <C-n> :call NumberToggle()<cr>
"set norelativenumber

" Open nerdtree when no file arg specified
autocmd VimEnter * if !argc() | NERDTree | endif

"CTRL-t to toggle tree view with CTRL-t
nmap <silent> <C-t> :NERDTreeToggle<CR>
nmap <silent> <F2> :NERDTreeFind<CR>
let NERDTreeShowHidden=1

map <F6> :vsplit let $VIM_DIR=expand('%:p:h')<CR>:terminal<CR>cd $VIM_DIR<CR>

"enable mouse support
set mouse=a

"check file change every 4 seconds ('CursorHold') and reload the buffer upon detecting change
set autoread
au CursorHold * checktime

"convert tabs to 4 spaces
set tabstop=4
"shift/indent also to 4 spaces
set shiftwidth=4
"auto indent (pressing enter, will indent)
set ai

"global clipboard for copy pasting between terminals
set clipboard=unnamedplus


"use the power line fonts
let g:airline_powerline_fonts = 1

"This will allow the airline plugin to load up as soon as you start editing a file
set laststatus=2

"to get colors working correctly.
set t_Co=256

"to hide the default mode (INSERT, NORMAL, etc)
set noshowmode 

" get rid of the `|` in the window splits (signifcant whitespace after \ )
set fillchars+=vert:\ 

"the following lines are appended from the setup to set RUST_SRC_PATH

set hidden
let g:racer_cmd="/Users/ibratoev/.cargo/bin/racer"
let g:syntastic_c_checkers = []
let g:syntastic_cpp_checkers = []
let g:syntastic_rust_checkers = []
autocmd BufWrite *.rs :silent! exec "!fcli tags vi --quiet --start-dir=/work &" | redraw!
let g:racer_experimental_completer = 1
au FileType rust nmap gd <Plug>(rust-def)
au FileType rust nmap gs <Plug>(rust-def-split)
au FileType rust nmap gx <Plug>(rust-def-vertical)
au FileType rust nmap <leader>gd <Plug>(rust-doc)

set autoindent
set cindent

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let g:rustfmt_autosave = 1

let g:syntastic_sh_checkers = ['shellcheck', 'sh']

let g:syntastic_python_python_exec = 'python3'


" Enable filetype plugins
filetype plugin on
filetype indent on
filetype plugin indent on

"Always show current position
set ruler

" Show matching brackets when text indicator is over them
set showmatch

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Enable syntax highlighting
syntax enable

" colorscheme desert
set background=dark

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

" Use spaces instead of tabs
set expandtab

" Be smart when using tabs ;)
set smarttab

" 1 tab == 4 spaces
set shiftwidth=4
set tabstop=4

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines
set hlsearch

autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/
autocmd BufWrite *.rs :silent! exec "!rusty-tags vi --quiet --start-dir=" . expand('%:p:h') . "&" | redraw!
autocmd BufRead *.rs :setlocal tags=./rusty-tags.vi;/,$RUST_SRC_PATH/rusty-tags.vi

let g:go_fmt_command = "goimports"
let g:go_auto_type_info = 1  

nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

map <F6> :vsplit let $VIM_DIR=expand('%:p:h')<CR>:terminal<CR>cd $VIM_DIR<CR>

set splitbelow
set splitright

:set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣

let g:airline#extensions#clock#format = '%Y %b %d %H:%M:%S'

let g:airline#extensions#clock#updatetime = 1000
set foldmethod=indent
set foldlevel=99
let g:rainbow_active = 1
let g:loaded_matchit = 1

