#!/usr/bin/python
import os


startup = {}
startup['x86'] = """
exec bgchanger
exec pasystray
exec nm-applet
exec conky
exec light-locker
"""

startup['arm'] = """
exec bgchanger
exec nm-applet
exec conky
exec mate-settings-daemon
exec mate-power-manager
exec light-locker
"""

browser = {}
browser['x86'] = 'google-chrome-stable'
browser['arm'] = 'chromium-browser'

def generate_conf():
    with open('/home/amentis/.config/amentis/i3/config.head') as head:
        s = head.read()
        s = s.replace('%AUTOSTART%', startup[os.environ['ARCH']])
        s = s.replace('%BROWSER%', browser[os.environ['ARCH']])
        return s


def write_conf():
    with open('/home/amentis/.config/i3/config', 'w') as target: 
        target.write(generate_conf())


if __name__ == '__main__':
    write_conf()

