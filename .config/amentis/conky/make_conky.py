#!/usr/bin/python
import os

assert os.environ['NETWORK_INTERFACE']
assert os.environ['DISK_DRIVE_0']

def get_disk_drive_line(drive):
    return ' {d} $color${{fs_used {d}}}/${{fs_size {d}}} ${{fs_bar 6 {d}}}'.format(d=drive)

def get_disk_drive_lines():
    l = []
    for k, v in os.environ.items():
        if 'DISK_DRIVE_' in k:
            l.append(get_disk_drive_line(v))
    return '\n'.join(l)

def generate_conf():
    with open('/home/amentis/.config/amentis/conky/conky.conf.head') as head:
        s = head.read()
        s = s.replace('%IF%', os.environ['NETWORK_INTERFACE'])
        s = s.replace('%DRIVES%', get_disk_drive_lines())
        return s


def write_conf():
    with open('/home/amentis/.config/conky/conky.conf', 'w') as target: 
        target.write(generate_conf())


if __name__ == '__main__':
    write_conf()

