export RUST_BACKTRACE='full'
export PKG_CONFIG_PATH="/usr/local/opt/openssl/lib/pkgconfig"
alias grep='grep --color=always'

export PATH="/usr/local/opt/openssl/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/openssl/lib"
export CPPFLAGS="-I/usr/local/opt/openssl/include"
export PATH="/usr/local/sbin:$PATH"
export PATH=$PATH:~/bin/openapitools/
export PATH=$PATH:~/go/bin/
export LANG=en_US.UTF-8
export LC_ALL=$LANG

# Get list of gnubin directories
export GNUBINS="$(find /usr/local/opt -type d -maxdepth 3 -follow -name gnubin -print)";

for bindir in ${GNUBINS[@]}; do
  export PATH=$bindir:$PATH;
done;

HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=$HISTSIZE
setopt notify
bindkey -e
zstyle :compinstall filename '/home/ibratoev/.zshrc'
autoload -Uz compinit
compinit
autoload bashcompinit
bashcompinit
# aliases
alias a='cat'
alias p='cp'
alias m='mv'
alias c='clear'
alias v='nvim'
alias vim='nvim'
alias h='cd ~/'
alias rt='cd /'
alias b='cd ../'
alias dl='rm -r '
alias ung='tar -xvzf '
alias unb='tar -xvjf '
alias unx='tar -xvJf '
alias ing='tar -cvzf '
alias inb='tar -cvjf '
alias inx='tar -cvJf '
alias l='ls'
alias la='ls -alhN --group-directories-first'
alias lel='ls -lh'
alias ca='cargo'
alias 'g+'='g++ -pedantic-errors -Wall'
alias ':q'='exit'
alias md=mkdir

# Git aliases
alias g='git'
alias ga='g add'
alias gb='g branch'
alias gd='g diff'
alias gds='gd --staged'
alias gf='g fetch'
alias gps='g push'
alias gpl='g pull'
alias gcm='g commit'
alias gch='g checkout'
alias gs='g status'
alias gm='g merge'
alias gst='g stash'
alias gl='g log --oneline'
alias gm='g merge'
alias gr='g reset'
alias grh='g reset --hard'

# kubectl aliases
alias k='kubectl'
alias kc='kubectl create'
alias kxps='kubectl expose'
alias krun='kubectl run'
alias kset='kubectl set'
alias kexp='kubectl explain'
alias kg='kubectl get'
alias ke='kubectl edit'
alias kd='kubectl delete'
alias kroll='kubectl rollout'
alias ks='kubectl scale'
alias kas='kubectl autoscale'
alias ktop='kubectl top'
alias kdes='kubectl describe'
alias kl='kubectl logs'
alias kexec='kubectl exec'
alias kcp='kubectl cp'
alias ka='kubectl apply'
alias kp='kubectl patch'
alias kr='kubectl replace'

alias cpdr='pwd | pbcopy'
alias pdr='cd $(pbpaste)'

function search() {
    find . -type f -name $1 | xargs grep $2 --color -n
}

export KUBE_EDITOR="/usr/local/bin/nvim"

export PS1="%n(%c)> "
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

source <(kubectl completion zsh)

if [[ -n "${NVIM_LISTEN_ADDRESS}" ]]
then
    # TODO update the path each time Vim has a major upgrade
    export VIMRUNTIME=/usr/share/vim/vim82
fi
ulimit -n 10240

